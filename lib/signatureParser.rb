require 'logger'

class SignatureParser
	def initialize()
	end 

	def check_signature(nodename,terms)
		#Copy the file to the /tmp directory to avoid the .sig files to stay in the project directory
		system 'cp '+@inputfilename+' /tmp/file.dol'
		#Generate the .sig files with hets -v2 -o sig <file>
		system 'hets -v2 -o sig /tmp/file.dol'
		file = "/tmp/file_"+nodename+".sig"
		properties = Hash.new
		File.open(file, "r") do |f|
			sig_file_content=String.new
			f.each_line do |line|
				type,property = parse_sig_line(line)
				if type != nil && property != nil then
					if properties[type] == nil then
						properties[type] = "#{property}"
					else
						properties[type] = properties[type]+",#{property}"
					end
				end
				#lines.$logger.debug(line)
				#sig_file_content+=line
			end
		end
		system 'find /tmp -name \*.sig -delete'
		#system 'rm /tmp/file.dol'
		resultSig = compareSignatures(terms,properties,"Class,ObjectProperty")
		return resultSig

	end

	private 
	
	def parse_sig_line(line)
		#If it's a Class or ObjectProperty entry
		if line.start_with?("Class","ObjectProperty") then 
			#Then identify the type
			if line.start_with?("Class") then 
				type = "Class"
			else
				type = "ObjectProperty"
			end
			#If the lines looks like "Anything: <anything#anything>" (case insensitive) then
			if line =~ /.*:\s<.*#.*>/i then
				#Split the input line to get an array that looks like ["what's before the #","what's after the # - the term"]
				splittedLine = line.split('#')
				#If there is terms in the array created
				if splittedLine!=nil && splittedLine.length > 0 then
					#Then get the term, identify the ontlology prefix and send back the type, the sanitized term and the ontology prefix
					term=splittedLine[1]
					if term != nil then
						term=term[0,term.length-2]
						ont = splittedLine[0].split("/").last
						return type,sanitize(term),ont
					end
				end
			else
				tmp = line.split(':')
				if tmp!=nil && tmp.length > 0 then
					tmp = tmp.last.delete("\n")
					return type,sanitize(tmp)
				end
			end
		end
	end
end