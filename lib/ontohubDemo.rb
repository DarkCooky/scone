#!/usr/bin/ruby
require_relative './ontohubUtil.rb'

# To run, enter the rails console : 
# $ bundle exec rails c
# Then, type the command :
# pry(main)> require "/vagrant/project/lib/ontohubDemo.rb"

time = Time.new
theorems = {}
ontohubUtil1 = OntohubUtil.new("/vagrant/project/resources/dol/tmp/Family_relationships_Inferring.dol","ontohubDemo",1,"ontohubDemoDolFile_Family_relationships_Inferring_"+time.strftime("%Y%m%d_%H%M%S")+".dol",theorems)
ontohubUtil1.execute()
theorems = {"CQ1"=>true,"CQ2"=>true,"CQ3"=>true}
ontohubUtil1 = OntohubUtil.new("/vagrant/project/resources/dol/tmp/Family_relationships_Mothers_are_female.dol","ontohubDemo",1,"ontohubDemoDolFile_Family_relationships_Mothers_are_female_"+time.strftime("%Y%m%d_%H%M%S")+".dol",theorems)
ontohubUtil1.execute()
theorems = {"CQ1"=>true,"CQ2"=>true,"CQ3"=>true,"CQ4"=>true}
ontohubUtil1 = OntohubUtil.new("/vagrant/project/resources/dol/tmp/Family_relationships_Relative.dol","ontohubDemo",1,"ontohubDemoDolFile_Family_relationships_Relative_"+time.strftime("%Y%m%d_%H%M%S")+".dol",theorems)
ontohubUtil1.execute()