#!/usr/bin/ruby
require 'logger'

class OntohubUtil  
	def initialize(file_path,repository_name,user_id,new_file_path,nodes_to_prove)  
		@file_path = file_path  
		@repository_name = repository_name  
		@user_id = user_id  
		@new_file_path = new_file_path  
		@nodes_to_prove = nodes_to_prove  
		#@inputfilename = inputfilename  
		@NodeIdMap = Hash.new
	end  

	def execute()
		user = User.find_by_id(@user_id)
		repository = Repository.find_by_name(@repository_name)
		if repository == nil then
			repository = Repository.new
			repository.name = @repository_name
			repository.save
		end
		$logger.debug 'Add the file '+@file_path+' to the repository '+@repository_name
		ontology_version = repository.save_file(@file_path, @new_file_path, 'Add the file '+@file_path+' to the repository '+@repository_name, user, do_not_parse: true)
		ontology_version.parse_full
		children = ontology_version.ontology.children
		@nodes_to_prove.each do |node,expected|
			children.each do |singleOntology|
				if singleOntology.name == node then
					@NodeIdMap[node] = singleOntology.id
				end
			end
		end
		$logger.debug "###################"
		prover = Prover.find_by_name('Pellet')
		prove_options = Hets::ProveOptions.new(prover: prover)
		@NodeIdMap.each do |node_name,node_id|
			child = Ontology.find(node_id)
			child.current_version.prove(prove_options)
			$logger.debug "proving node #{node_name} (id #{node_id})"
		end
		#signature = ontology_version.ontology.symbols.where(kind: 'Class')
		#$logger.debug "Signature : #{signature}"
	end

end