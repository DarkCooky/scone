Feature: Family relationships  
  In order to support my research 
  the user should be able query my data for family relationships using 
 	"male", "female", "parent of", "grandparent of", 
 	"father of", "mother of", "older than" 
 
Background:
  * Language %OWL
  * Test the ontology <file:///data/resources/Presentation/familyRel.owl>

Scenario: Relative age between family members
The parenthood relation entail an ordering of age. 

  Given Chris is a parent of Dora. 
  And Amy is a parent of Chris. 
  And Amy is a parent of Berta.
  Then infer Chris is older than Dora. 
  And infer Amy is older than Dora. 
  And don't infer Berta is older than Dora. 
  And don't infer Dora is older than Dora. 
	
Scenario:  Inferring various family relationships
In this scenario we test the definitions of "mother of", "grandparent", 
and "father".   

  Given John is a parent of Mary.
  And Sue is a mother of John. 
  Then infer that Sue is a grandparent of Mary. 
  Given John is male. 
  Then infer that John is a father. 
  Given a mother is defined as a female, who is a parent of some thing. 
  Then infer that Sue is a mother. 
  And the scenario is consistent. 
		
  
Scenario: Mothers are female

  Given Jill is the mother of Chris. 
  Given Jill is not female.

  Then the scenario is inconsistent.
