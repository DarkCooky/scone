require 'logger'

# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

class SconeParser
  
  attr_reader   :imports, # import declarations
                :assumptions, # additional assumptions
                :blockAssumptions, #additional block assumptions
                :questions # competency questions

  def initialize()
    @imports = []
    @assumptions = []
    @blockAssumptions = []
    @questions = []
  end
  
  def addImport(import)
    @imports << import
  end
  
  def addAssumption(assumption)
    @assumptions << assumption
  end
  
    def addBlockAssumption(assumption)
    @blockAssumptions << assumption
  end
  
    def addQuestion(question)
    @questions << question
  end
  
  # performs only logging
  def processDeclaration(*decl)
    decl.each do |dec|
      $logger.debug(dec)
    end 
  end
  
  def infer_sentence(sentence)
    
  end
  
end
