

require 'logger'

## This class generates dol files from the generated model.
class DOLFileBuilder 
  
  
  attr_accessor :dolFile, # file to generate
                :model, # model of the file

  @@import_node = "Import"
  @@combined_import_node = "CombinedImports"
  @@cq_node = "CompetencyQuestion"
  @@scenario_node = "Scenario"
  
  
  def initialize(filename, model)

    $logger.debug "model is : \n #{model}"

    @filename = filename
    @model = model
    @imports = Hash.new
    @cqsCounter = 0
  end
  
  def buildFile()
    File.open(@filename, 'w') do |file|
      @dolFile = file
      generateDOLTokens()
      @proveNodes = generateNodeForCQ(@model[:prove])
      @disproveNodes = generateNodeForCQ(@model[:disprove])
      declarePrefixes()
      declareLanguage()
      declareImportStatements()
      declareCombinedImportsStatement()
      declareScenarioAxioms()
      @cqsCounter = 0
      declareNodesForCQs(@proveNodes)
      declareNodesForCQs(@disproveNodes)
      declareCompetencyQuestions(@proveNodes)
      declareCompetencyQuestions(@disproveNodes)

      $logger.debug "============"
      $logger.debug @model[:prove]
      $logger.debug @proveNodes
          $logger.debug "============"
    end
    [@proveNodes , @disproveNodes , @@scenario_node]
  end
  
  def generateDOLTokens()
    generateImportTokens()
  end

  def generateImportTokens()
    @model[:imports].each_with_index do |it, index|
    @imports["#{@@import_node}#{index}"] = it
    end
  end


  def declarePrefix()
    s = """
    %prefix(
 : <file:///data/resources/DOLFiles/localmanuallyGeneratedTranslation#>
     )% \n"""
     @dolFile.write s
     jumpLine
  end

  def declarePrefixes()
    s = "%prefix("
    @model[:prefixes].each { |prefix, uri|
        s += ( "\n" + prefix +": " + uri)
    }
    s += "\n #{@model[:scenario_prefix]} : <#{@model[:file_location]}>"
    s += "\n )% \n"

    @dolFile.write(s) 
    jumpLine()
  end
  
  def declareLanguage()
    @dolFile.write("logic #{@model[:language_id]} \n")
    jumpLine()
  end
  
  def declareImportStatements()
    @imports.map { |key, value|  "ontology #{key} = #{value} \n"}
            .each {|it| @dolFile.write it}
    jumpLine()
  end

  def declareCombinedImportsStatement()
    combined = @imports.each_key.inject() { |s,import|  "#{s} and #{import} "}
    s1 = "ontology #{@@combined_import_node} = #{combined} \n"
    dolFile.write(s1)
    jumpLine()
  end
  
  def declareScenarioAxioms()    
    @dolFile.write("ontology #{@@scenario_node} = #{@@combined_import_node} then \n")
    @model[:newTerms].map { |axiom| "\t#{axiom} \n" }
                       .each {|it| @dolFile.write it}   
    @model[:axioms].map { |axiom| "\t#{axiom} \n" }
                       .each {|it| @dolFile.write it}

    # @model.blockaxioms.each{ |block|
    # @dolFile.write block.split(/\n/).inject("") { |prettyprint, obj| "#{prettyprint}\t#{obj}\n"  }
    # }
    @dolFile.write "end\n"
    jumpLine()
  end

  def generateNodeForCQ(cqs)
    map = Hash.new
    cqs.each_with_index { |question , index|
      @cqsCounter += 1
      map["#{@@cq_node}#{@cqsCounter}"] = question
    }
    map
  end

  def declareNodesForCQs(nodeMap)
    s = ""
    nodeMap.each { |nodeName , v |
      s += "ontology #{nodeName} = #{@@scenario_node} \n"
    }
    @dolFile.write s
    jumpLine
  end
  
  def declareCompetencyQuestions(nodeMap)

      nodeMap.each { | node , question|
      @cqsCounter += 1
      s = StringIO.new
      s << "ontology #{@@scenario_node}#{@@cq_node}#{@cqsCounter} = #{node} then %implies \n"
      s << "\t#{question}"
      @dolFile.write "#{s.string} \n end \n"
    }
    jumpLine()
  end
  
  def jumpLine()
    @dolFile.write("\n")
  end

end
