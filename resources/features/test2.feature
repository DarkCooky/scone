  @infer @parser
  Feature: Test feature2

  Scenario:

    Then infer John is an B1.
    Then infer John is an B2. 
    Then infer every B3 is a subclass of B2.
    Then infer every B1 is a B3
    Then infer no B1 is an B3
    Then infer that B1 and B3 are disjoint

    Then infer John does love Mary
    
    #Relation
    Then infer John is married to Mary

    Then infer John hasn't as wife Jane

    Then infer Josh is the same as Mike
    Then infer Rob and Andrew and John are different

    Then infer B1 is defined as thing that we can parse
    Then infer B2 is enumerated as John and Robert and Mike

    Then infer every John is a mammal which  is a father of some cat.
    
    Then don’t infer that John does loves Mary.
   