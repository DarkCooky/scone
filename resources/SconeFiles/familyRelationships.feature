Feature: Family relationships  
  In order to support my genealogical research 
  As end-user  
  I should be able query my data for family relationships using 
 	"male", "female", "parent of", "grandparent of", 
 	"father of", "mother of", "older than" 
 
Background:
  * Language #OWL
  * Load the ontology <https://example.org/geneology.owl> 

Scenario: 
  Given Chris is a parent of Dora. 
  And Amy is a parent of Chris. 
  And Amy is a parent of Berta.
  Then infer Chris is older than Dora. 
  And infer Amy is older than Dora. 
  And don't infer Berta is older than Dora. 
  And don't infer Dora is older than Dora. 
	
Scenario:   
  Given John is a parent of Mary.
  And Sue is a mother of John. 
  Then infer that Sue is a grandparent of Mary. 
  Given John is male. 
  Then infer that John is the father of Mary. 
  Given a mother is defined as a female, who is a parent of 
  some person. 
  Then infer that Sue is a mother. 
  And the ontology is consistent. 
		
Scenario:
  Given Jeff is the father of Chris. 
  And Chris is older than Jeff. 
  Then the ontology is inconsistent. 