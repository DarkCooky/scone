
  Feature: Test feature

  Background: 
  * Load the ontology <https://ontohub.org/sandbox/cycle.owl> 

  Scenario: 
  	Given %owl 
    """
    Individual: John 
    """
    Given John is an B1. 
    Then infer John is an B2. 

